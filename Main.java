package org.example;


public class Main {
    public static void main(String[] args) {
        int[] numbers = {9, 1, 8, 3, 4, 2, 1, 0};
        for (int x : numbers)
            System.out.print(x + " ");
        boolean isIncreasing = true;
        for (int i = 0; i < numbers.length - 1; i++) {
            if (numbers[i] < numbers[i + 1]) {
                System.out.println("Массив не строго возрастающий");
                isIncreasing = false;
                break;
            }
        }
        if (isIncreasing) System.out.println("Массив возрастающий");
        for (int i = 0; i < numbers.length; i++) {
            if (i % 2 != 0) numbers[i] = 0;
        }
        for (int x : numbers)
            System.out.print(x + " ");
    }

}